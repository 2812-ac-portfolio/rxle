FROM registry.gitlab.com/2812-ac-portfolio/rxle/dotnet/sdk:6.0

ENV ACCEPT_EULA=y

RUN apt-get update --fix-missing
RUN apt-get install -y gnupg2 openssh-client

RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN curl https://packages.microsoft.com/config/ubuntu/20.04/prod.list | tee /etc/apt/sources.list.d/msprod.list

RUN curl -fsSL https://deb.nodesource.com/setup_14.x | bash -

RUN apt-get install -y mssql-tools unixodbc-dev nodejs jq

RUN curl -fsSL https://get.docker.com -o get-docker.sh
RUN sh get-docker.sh
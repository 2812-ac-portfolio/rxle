# Rxle

## Overview

Rxle is a faux healthcare and prescription management solution that seeks to demonstrate modern development techniques and processes. This project will include a CLI, Rest API, GraphQL API, .Net library, Angular frontend, TSQL database, VuePress documentation site, and configurations for running the solution using docker images (including a docker-compose.yml template), helm charts, or IIS.

**Quick Note**: This project uses GitLab container registry and docker fairly heavily for CI/CD pipelines and containerized development. To learn more about using the container registry in GitLab, you can view their documentation [here](https://docs.gitlab.com/ee/user/packages/container_registry/). In short, if you'd like to push changes then you will need to create a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) in GitLab with the read_registry and write_registry scopes.

## Dev Container Setup

This solution is developed primarily within containers using Docker Desktop and the Visual Studio Code extension 'Remote Development'. This means anyone pulling the solution in Visual Studio Code with the specified extension (`ms-vscode-remote.vscode-remote-extensionpack`) should be prompted to open the project within a container. This will start up all the services necessary to develop changes and will pre-populate the database with test data. While working within the container, a number of quick actions will become available. These quick actions can be reviewed by navigating to the folder ~/Scripts/QuickActions. To update the dev environment, make changes to `./Docker/dev-env.dockerfile`. To push an updated version of the environment to GitLab, run the script `./Scripts/DevEnv/update-dev-env-image.bash` and provide a version number. E.x.: `./Scripts/DevEnv/update-dev-env-image.bash 1.0.0`.

**Important Note**: For database access to work in the dev container, you will need to setup an environment variable named LOCAL_DB_PASSWORD on your machine. This variable is used by Docker Compose to create the SQL Server instance and is shared with the dev container under the variable name Rxle_Repository__Password. You can generate a new password into this variable by running the script: ~/Scripts/Prep/generate-and-set-local-db-password.ps1.

**Note Regarding Pushing New Base Container Images (node, dotnet/sdk)**: If a new version of node or the .Net SDK is needed, you can use the `./Scripts/DevEnv/update-node-docker-image.bash` or `./Scripts/DevEnv/update-dotnet-sdk-docker-image.bash` scripts. These scripts require 1 parameter: the version of the image to pull. E.x.: `./Scripts/DevEnv/update-node-docker-image.bash 16`

## Pages Setup

This solution leverages GitLab pages and VuePress for displaying system documentation. Upon a successful merge to develop, the GitLab CI configuration will automatically create a pipeline to build and upload the documentation. This documentation is available [here](https:/gitlab.io/2812-ac-portfolio/rxle). To add a new page, create a new markdown document somewhere within the folder `./Documentation/src` or one of its sub-folders (excluding .vuepress). New sub-folders may be created here as well. When the markdown document is complete and ready to be viewed within the system, modify the .themeConfig.sidebar section of the file `./Documentation/src/.vuepress/config.js`, adding a new section for the page.

The debug configuration `Launch Documentation Site` will start up the local VuePress server to preview the documentation.
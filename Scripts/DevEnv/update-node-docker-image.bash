echo "Logging into GitLab... You may be prompted for credentials."
docker login registry.gitlab.com

echo "Pulling container image..."

docker pull node:$1

baseTag=registry.gitlab.com/2812-ac-portfolio/rxle
newTag=$baseTag/node:$1
latestTag=$baseTag/node:latest

echo "Pushing new tags..."

docker tag node:$1 $newTag
docker tag node:$1 $latestTag

docker push $newTag
docker push $latestTag
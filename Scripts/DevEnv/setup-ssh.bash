cat /importmisc/ssh/id_rsa > ~/.ssh/id_rsa
cat /importmisc/ssh/id_rsa.pub > ~/.ssh/id_rsa.pub
cat /importmisc/ssh/known_hosts > ~/.ssh/known_hosts

chmod 400 ~/.ssh/id_rsa
chmod 700 ~/.ssh

eval `ssh-agent` > /dev/null
ssh-add -q ~/.ssh/id_rsa
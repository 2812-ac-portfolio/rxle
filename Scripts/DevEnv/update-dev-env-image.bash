echo "Logging into GitLab... You may be prompted for credentials."
docker login registry.gitlab.com

cd Docker

baseTag=registry.gitlab.com/2812-ac-portfolio/rxle/dev-env
newTag=$baseTag:$1
latestTag=$baseTag:latest

echo "Building container image..."

docker build -t $newTag -f dev-env.dockerfile .

echo "Publishing updated images..."

docker tag $newTag $latestTag

docker push $newTag
docker push $latestTag
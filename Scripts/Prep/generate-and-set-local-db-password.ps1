$password = New-Object -TypeName PSObject
$password | Add-Member -MemberType ScriptProperty -Name "Password" -Value { ("!@#$%^&*0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz".tochararray() | Sort-Object {Get-Random})[0..8] -join '' }
[Environment]::SetEnvironmentVariable("LOCAL_DB_PASSWORD", "$($password.Password)", 'User')
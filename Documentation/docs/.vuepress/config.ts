import { defineUserConfig } from 'vuepress'
import type { DefaultThemeOptions } from 'vuepress'

export default defineUserConfig<DefaultThemeOptions>({
  lang: 'en-US',
  title: 'Rxle Documentation',
  description: 'Documentation site for the Rxle project',
  base: '/rxle/',

  theme: '@vuepress/theme-default',
  themeConfig: {
    sidebar: [{
      link: '/',
      text: 'Overview',
    }, {
      link: '/planning',
      text: 'Planning',
      children: [{
        link: '/planning/initial-requirements',
        text: 'Initial Requirements'
      }]
    }]
  }
})
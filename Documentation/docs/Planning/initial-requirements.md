# Initial Rxle Requirements

* Allow users to view prescriptions
    * Data point details:
        * Fulfilling Pharmacy
        * \# of Refills left
        * Refill Frequency
        * Prescriber
        * Name of Medication
        * Medication Size (e.x. 16 mg)
        * Prescription Size (e.x. 2 pills)
        * Dosage Schedule
        * Notes/Instructions
        * Date of Prescription
* Allow user's medical professionals to create/manage prescriptions
* Allow users to connect with medical professionals
* Allow users to connect with organizations
* Allow creation of user accounts
    * Data point details:
        * Email Address
        * Name
        * Phone Number
        * Email
        * Password Hash
        * Addresses
            * Address Line 1
            * Address Line 2
            * City
            * State
            * Zip
        * Insurance Cards
            * Provider Name
            * Image
            * Member #
            * Group #
            * Contact #
* Allow creation of organizations
    * Locations
        * Address Line 1
        * Address Line 2
        * City
        * State
        * Zip
    * Name
    * Organization Type
        * Insurance Company
        * Pharmacy
        * Medical Practice
* Allow organizations to register members
* Allow medical professionals to send prescriptions to pharmacies
* Allow pharmacies and medical professionals to send claims to insurance companies
    * Data point Details:
        * Individual
            * Name
            * Insurance Information
                * Member #
                * Group #
                * Contact #
* Allow user to consent to their information being shared
    * Allowances
        * Full access to share
        * Access to share to an individual
        * Access to share to an organization
* When lacking user consent, notify user and ask for permission each time a prescription or claim needs been forwarded
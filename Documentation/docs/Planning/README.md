# Planning Overview

This project includes a number of planning artifacts that help convey what the solution was built to provide.

## Initial Requirements Document

This document is intended to help establish the needs as initially stated by the requesting party. It seeks to establish the major data points that will be used and describe key functionality that must be enabled.

This document can be seen [here](./initial-requirements.md).
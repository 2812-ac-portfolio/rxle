# Rxle Documentation

## Overview

Rxle is a faux healthcare and prescription management solution that seeks to demonstrate modern development techniques and processes. This project will include a CLI, Rest API, GraphQL API, .Net library, Angular frontend, TSQL database, VuePress documentation site, and configurations for running the solution using docker images (including a docker-compose.yml template), helm charts, or IIS.